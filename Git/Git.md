- [volver a inicio](https://gitlab.com/jlc.rodriguez/capacitaciones-yavirac-mlab-2021/-/blob/master/README.md)

# Menu
- [Que es Git?](#git)
- [Comandos de Git en Consola](#comandos)
- [Clientes Git](#clientes)
- [Clonación de proyecto por consola y por cliente](#clonación)
- [Commits por Consola y por Cliente Kraken](#commits)
- [Ramas desde Kraken](#ramas)
- [Merge](#merge)

# Git

- [Ir a inicio](#menu)

Es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando estas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora incluyendo coordinar el trabajo que varias personas realizan sobre archivos compartidos en un repositorio de código.

# Comandos

- [Ir a inicio](#menu)

## **Configuración Básica**

Configurar Nombre que salen en los commits

	git config --global user.name "dasdo"

Configurar Email

	git config --global user.email dasdo1@gmail.com
Marco de colores para los comando

	git config --global color.ui true

## **Iniciando repositorio**

Iniciamos GIT en la carpeta donde esta el proyecto

	git init
Clonamos el repositorio de github o bitbucket

	git clone <url>
Añadimos todos los archivos para el commit

	git add .
Hacemos el primer commit

	git commit -m "Texto que identifique por que se hizo el commit"
subimos al repositorio

	git push origin master

## **GIT CLONE**

Clonamos el repositorio de github o bitbucket

	git clone <url>
Clonamos el repositorio de github o bitbucket ?????

	git clone <url> git-demo

## **GIT ADD**

Añadimos todos los archivos para el commit

	git add .
Añadimos el archivo para el commit

	git add <archivo>
Añadimos todos los archivos para el commit omitiendo los nuevos

	git add --all 
Añadimos todos los archivos con la extensión especificada

	git add *.txt
Añadimos todos los archivos dentro de un directorio y de una extensión especifica

	git add docs/*.txt
Añadimos todos los archivos dentro de un directorios

	git add docs/

## **GIT CLONE**
Clonamos el repositorio de github o bitbucket

	git clone <url>
Clonamos el repositorio de github o bitbucket ?????

	git clone <url> git-demo

## **GIT ADD**
Añadimos todos los archivos para el commit

	git add .
Añadimos el archivo para el commit

	git add <archivo>
Añadimos todos los archivos para el commit omitiendo los nuevos

	git add --all 
Añadimos todos los archivos con la extensión especificada

	git add *.txt
Añadimos todos los archivos dentro de un directorio y de una extensión especifica

	git add docs/*.txt
Añadimos todos los archivos dentro de un directorios

	git add docs/

## **GIT COMMIT**
Cargar en el HEAD los cambios realizados

	git commit -m "Texto que identifique por que se hizo el commit"
Agregar y Cargar en el HEAD los cambios realizados

	git commit -a -m "Texto que identifique por que se hizo el commit"
De haber conflictos los muestra

	git commit -a 
Agregar al ultimo commit, este no se muestra como un nuevo commit en los logs. Se puede especificar un nuevo mensaje

	git commit --amend -m "Texto que identifique por que se hizo el commit"

## **GIT PUSH** 
Subimos al repositorio

	git push <origien> <branch>
Subimos un tag

	git push --tags

## **GIT LOG**
Muestra los logs de los commits

	git log
Muestras los cambios en los commits

	git log --oneline --stat
Muestra graficos de los commits

	git log --oneline --graph

## **GIT BRANCH**
Crea un branch

	git branch <nameBranch>
Lista los branches

	git branch
Comando -d elimina el branch y lo une al master

	git branch -d <nameBranch>
Elimina sin preguntar

	git branch -D <nameBranch>

    GIT BRANCH
Crea un branch

	git branch <nameBranch>
Lista los branches

	git branch
Comando -d elimina el branch y lo une al master

	git branch -d <nameBranch>
Elimina sin preguntar

	git branch -D <nameBranch>

# Clientes

- [Ir a inicio](#menu)

* Cliente
<img src="clliente1.jpg" width="500" >


# Clonación de proyecto por consola y por cliente

- [Ir a inicio](#menu)

* Por Consola

    En la consola ponemos git clone y el link del repositorio que queremos clonar 
<img src="consola1.jpg" width="500" >

* Por Cliente 

En kraken damos click en clonar ponemeos la URl del archivo que queremos clonar y donde lo queremos ubicar

# Commits por Consola y por Cliente Kraken

- [Ir a inicio](#menu)

* Por Consola

<img src="commit1.jpg" width="500" >

* Cliente kraken

<img src="commitk1.jpg" width="500" >

<img src="commitk2.jpg" width="500" >


# Ramas desde Kraken

- [Ir a inicio](#menu)

* Aqui podemos observar las ramas de nuestro proyecto echo en clase 

<img src="rama1.jpg" width="500" >

# Merge

- [Ir a inicio](#menu)

Aqui podemos observar el **Merge** de nuestro proyecto echo en clase  

<img src="merch1.jpg" width="500" >
