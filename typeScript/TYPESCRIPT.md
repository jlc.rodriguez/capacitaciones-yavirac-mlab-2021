- [volver a inicio](https://gitlab.com/jlc.rodriguez/capacitaciones-yavirac-mlab-2021/-/blob/master/README.md)

# Menu
- [TypeScript](#ypeScript)
- [Caracteristicas](#caracteristicas)
- [Crear y compilar un archivo ](#crear_y_compilar_un_archivo)
- [Editores](#editores)
- [Que podemos hacer con TypeScript](#que_podemos_hacer_con_TypeScript)
- [Javascrip en POO](#javascrip_en_POO)

# TypeScript

- [Ir a inicio](#menu)

TypeScript es un lenguaje de programación de alto nivel que implementa muchos de los mecanismos más habituales de la programación orientada a objetos, pudiendo extraer grandes beneficios que serán especialmente deseables en aplicaciones grandes, capaces de escalar correctamente durante todo su tiempo de mantenimiento.

La característica fundamental de TypeScript es que compila en Javascript nativo, por lo que se puede usar en todo proyecto donde se esté usando Javascript. Dicho con otras palabras, cuando se usa TypeScript en algún momento se realiza su compilación, convirtiendo su código a Javascript común. El navegador, o cualquier otra plataforma donde se ejecuta Javascript, nunca llegará a enterarse que el código original estaba escrito en TypeScript, porque lo único que llegará a ejecutar es el Javascript resultante de la compilación.

En resumen, TypeScript es lo que se conoce como un "superset" de Javascript, aportando herramientas avanzadas para la programación que traen grandes beneficios a los proyectos.

  <img src="tipe1.jpg" width="500" >

# Caracteristicas

- [Ir a inicio](#menu)

* Una serie de funcionalidades de JavaScript 5, que es considerada hoy en día como un estándar y es la que todos los navegadores comprenden, incluso Node.js a nivel de servidor lo comprende.
 * Como JavaScript ha seguido creciendo y apareció ECMAScript 6 (ES6), que añade nuevas funcionalidades, y que también está englobado dentro de TypeScript.
 * Añade el uso un tipado muy estricto, en lugar del tipado dinámico de JavaScript, para intentar solucionar una serie de problemas. Aunque estos tipados no son requeridos, es recomendable utilizarlos para tener un código mucho más limpio.
 * Añade la posibilidad de poder utilizar interfaces, para poder definir nuestros propios tipos o intentar aplicar programación orientada objetos luego.
 * También añade la funcionalidad de genérica, que permite poder definir funciones que sean reutilizables, independientemente del tipo de datos que vayamos a tratar.

# Crear y compilar un archivo 

- [Ir a inicio](#menu)

El archivo TypeScript lo creas con cualquier editor de texto para programadores, como habitualmente haces con cualquier otro lenguaje. Solo que generalmente usarás la extensión ".ts".

Algo muy importante: Cualquier código Javascript compila en TypeScript. Esto quiere decir que el código que tenemos en Javascript lo podemos usar también perfectamente dentro de Javascript. Por tanto, para trabajar con TS podemos usar perfectamente los conocimientos que tenemos en Javascript, agregando diversas ayudas que nos ofrece TypeScript.

Este sería un posible código TypeScript donde hemos colocado básicamente unos cuantas declaraciones de tipos en variables. Además de una función que se ha declarado devuelve "void" (nada).

  <img src="tipescrip1.jpg" width="500" >

# Editores

- [Ir a inicio](#menu)

Una de las cosas que debemos de conseguir cuando vamos a programar con TypeScript es un editor que lo soporte. El motivo es que los editores son capaces de compilar el código TypeScript a la vez que se está escribiendo, informando de errores en el código en el instante de su creación, lo que ahorra mucho tiempo y facilita el flujo de desarrollo.

Te informará de asuntos como:

Una variable con tipado estático a la que se le intenta cargar un dato de otro tipo
Una función que devuelve un valor de un tipo distinto al que debería
Una función a la que no se le están pasando los valores de tipos correctos, o los objetos de clases correctas
Un objeto sobre el que se intentan invocar métodos privados
Y la lista no para de crecer…
Además, al tipar las variables seremos capaces de obtener muchas más ayudas "intellisense", como métodos invocables sobre cadenas o números, métodos que contiene un objeto, etc.

Existen editores que ya incorporan las ayudas para la programación en TypeScript, como Visual Studio o Visual Studio Code (Este último un editor ligero y gratuito para cualquier uso). Otros editores como Vim, Atom, Sublime Text o Brackets necesitarán la instalación de plugins especiales para poder aportar todas las ayudas de TypeScript.

# Que podemos hacer con TypeScript

- [Ir a inicio](#menu)

Ya que TypeScript esta hecho con JavaScript, nos permite realizar o ser parte con otras tecnologías en muchos proyectos que JavaScript nos permite, como por ejemplo:

* Aplicaciones Móviles (Ionic, React Native, etc.)
* Páginas Web
* Sistemas Webs
* Librerías que podemos alojarlas en el repositorio NPM
* Crear aplicaciones de lado del servidor en Node JS
* Crear aplicaciones Desktop en Electron JS
* Crear interfaces y funcionalidades en React JS
* Crear interfaces y funcionalidades en Vue JS
* Crear interfaces y funcionalidades en Angular
* Crear componentes y módulos.
* Consumir REST APIs
* Entre muchas otras cosas más.

# Javascrip en POO

- [Ir a inicio](#menu)

Ahora que entendemos los conceptos de POO, vamos a ver mas a fondo la POO en JavaScript.

JavaScript es diferente a los otros lenguajes, ya que los conceptos como herencia, polimorfismo y encapsulamiento debido a su naturaleza no existen como tal; por ejemplo el encapsulamiento en java se maneja declarando una propiedad o método como private, qué se le conoce como modificadores de acceso, bueno en JS no existen modificadores de acceso, entonces, ¿no hay forma de encapsular en JS? la respuesta es sí, pero no de esa forma, para eso se utilizan las closures (pero ya hablaremos después de ellas, pero si eres impaciente y quieres saber mas.

En los fundamentos de POO vimos un poco de clases, herencia, etc.; a pesar de que nos referimos a ellas como clases, realmente son objetos, por eso se les conoce como fake class, pero vamos por pasos… primero veamos cómo funciona JS, y para entenderlo es viendo cadena de prototipos.

* **Constructor**

En POO es el método encargado de crear nuestro objeto e inicializar sus propiedades, en el momento que instanciamos una clase automáticamente se ejecuta su constructor.

  <img src="po1.jpg" width="500" >

* **Propiedades**

Las propiedades definen el estado de nuestro objeto, en las clases tenemos dos formas de inicializarlas:
Cuando se ejecuta el constructor
Por medio de un método set (previamente declarado)
En esta versión de JavaScript (ES5) no se contaba con modificadores de acceso para indicar que la propiedad es privada.

  <img src="po2.jpg" width="500" >

* **Métodos**

Los métodos son muy parecidos a las propiedades, solo que estas son funciones lo cual se encarga de modelar el comportamiento que va a tener nuestro objeto, al igual que las propiedades los métodos se pueden acceder directamente desde cualquier parte.

  <img src="po3.jpg" width="500" >

* **Herencia**

La herencia nos ayuda a modelar los objetos de una mejor manera, cuando aplicamos herencia de una clase a otra, lo que realmente estamos haciendo es que nuestra clase contenga todos los atributos y métodos de la clase padre.
Uno de los límites que tiene JavaScript, es que sólo permite herencia simple.

  <img src="po5.jpg" width="500" >

  <img src="po4.jpg" width="500" >

* **Encapsulamiento**

Se le llama encapsulamiento cuando la clase principal hereda los métodos de la clase padre, sin necesidad de saber cómo funcionan, sólo tiene que definir las cosas que desea cambiar.

  <img src="po6.jpg" width="500" >

* **Abstracción**

Veamos la definición que nos da wikipedia:
La abstracción consiste en aislar un elemento de su contexto o del resto de los elementos que lo acompañan. En programación, el término se refiere al énfasis en el “¿que hace?” más que en el “¿cómo lo hace?”. [ref]
Se refiere a poder tomar un objeto de la realidad y poderlo representar mediante una clase, el poder tomar ese objeto y el abstraer su comportamiento y sus propiedades para programáticamente poder crearlo y modificarlo.

* **Polimorfismo**

El polimorfismo: En la POO es la capacidad de crear una propiedad, método o un objeto que tenga más de una forma.
Pero en JavaScript realmente no tenemos interfaces como en otros lenguajes de programación, pero tenemos superclases y subclases, para entenderlo veamos el siguiente ejemplo:

  <img src="po7.jpg" width="500" >
