- [volver a inicio](https://gitlab.com/jlc.rodriguez/capacitaciones-yavirac-mlab-2021/-/blob/master/README.md)

# MENU
- [Que es Gitlab?](#gitlab)
- [Como accedo?](#acceso)
- [Creacion de Repositorios](#repositorios)
- [Grupos](#grupos)
- [Subgrupos](#subgrupos)
- [Creacion de Issues](#issues)
- [Creacion Labels](#labels)
- [Permisos](#permisos)
- [Agregar Miembros](#miembros)
- [Commit](#commit)
- [Ramas](#ramas)

# Gitlab

- [Volver a Inicio](#menu)

Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.

GitLab es una suite completa que permite gestionar, administrar, crear y conectar los repositorios con diferentes aplicaciones y hacer todo tipo de integraciones con ellas, ofreciendo un ambiente y una plataforma en cual se puede realizar las varias etapas de su SDLC/ADLC y DevOps.

# Acceso

[Volver a Inicio](#menu)

En mi caso accedi con mi correo institucional 

* Primero me registre con el mismo

  <img src="acceso1.jpg" width="199" >

*  Luego ingrse a mi perfil ya creado

   <img src="acceso2.jpg" width="199" >

# Repositorios

[Volver a Inicio](#menu)

* En la pagina principal de Gitlab, tenemos que dar click en el boton **"Nuevo Proyecto"**

<img src="crear-repositorios1.jpg" width="500" >

* Nos aparecera la opcion **"Crear Proyecto en Blanco"** le damos click 

<img src="crear-repositorios2.jpg" width="500" >

* Luego tendremos que configurar a nuestro criterio las opciones que le vamos a dar a nuestro repositorio

<img src="crear-repositorios3.jpg" width="500" >

* Finalmente se habra creado nuestro repositorio

<img src="crear-repositorios4.jpg" width="500" >

# Grupos

[Volver a Inicio](#menu)

* La creacion de grupos es muy parecida a la de repositorios das click en nuevo grupo y selecionas las opciones a criterio propio

<img src="grupo1.jpg" width="500" >

* Nuevo grupo creado

<img src="grupo2.jpg" width="500" >

# Subgrupos

[Volver a Inicio](#menu)

* Nos dirigimos al boton subgrupo y le damos click

<img src="subgrupo1.jpg" width="500" >

* Y de la misma forma que creamos nuestro grupo le damos opciones a nuestro subgrupo

<img src="subgrupo2.jpg" width="500" >

# Issues

[Volver a Inicio](#menu)

* En nuestro repositorio creado nos dirigimos hacia la opcion **Issues** damos click 

<img src="issues1.jpg" width="500" >

* Damos clik en **Nuevo Issue**

<img src="issues2.jpg" width="500" >

* De igual manera que con los anteriores ejemplos le damos a las opciones de nuestra preferencia 

<img src="issues3.jpg" width="500" >

# Labels

[Volver a Inicio](#menu)

* En nuestro issue creado tenemos la opcion de designar **Labels**

<img src="label1.jpg" width="500" >

* Damos click en **Nuevo Label**

<img src="label2.jpg" width="500" >

* Le damos las opciones que queremos que tenga 

<img src="label3.jpg" width="500" >

* Ya creado nuestro Label podremos asinarlo a nuestros issues 

<img src="label4.jpg" width="500" >

<img src="label5.jpg" width="500" >

# Permisos

[Volver a Inicio](#menu)

Los usuarios tienen diferentes habilidades según el nivel de acceso que tengan en un grupo o proyecto en particular. Si un usuario está tanto en el grupo de un proyecto como en el proyecto mismo, se usa el nivel de permiso más alto.

En proyectos públicos e internos, la función de invitado no se aplica. Todos los usuarios pueden:

* Crea problemas.
* Dejar comentarios.
* Clona o descarga el código del proyecto.

Cuando un miembro abandona el proyecto de un equipo, todos los problemas y solicitudes de combinación asignados se anulan automáticamente.

Los administradores de GitLab reciben todos los permisos.

Para agregar o importar un usuario, puede seguir la documentación de los miembros del proyecto .

* **Principios detrás de los permisos**

* **Permisos de usuario de toda la instancia**

    De forma predeterminada, los usuarios pueden crear grupos de nivel superior y cambiar sus nombres de usuario. Un administrador de GitLab puede configurar la instancia de GitLab para modificar este comportamiento.

* **Permisos de los miembros del proyecto**

    Si bien Mantenedor es el rol de nivel de proyecto más alto, algunas acciones solo las puede realizar un propietario de grupo o espacio de nombres personal, o un administrador de instancia, que recibe todos los permisos.

# Miembros

[Volver a Inicio](#menu)

En nuestro repositorio tenemos la opcion de agregar miebros a nuestro proyecto, damos click en **Members** y podremos agregar a otro miembro en Gitlab

<img src="miembros1.jpg" width="500" >



Para crear un Board de problemas de flujo de trabajo, simplemente cree etiquetas para cada etapa de su flujo de trabajo y agréguelas como listas en un tablero. Una vez que haya etiquetado un problema, aparecerá automáticamente en la lista. Cuando el problema esté listo para pasar a la siguiente etapa, simplemente arrástrelo y suéltelo en la siguiente lista. También puede actualizar las etiquetas directamente en el problema y sus cambios aparecerán automáticamente en el Board.

Su flujo de trabajo podría verse así:

    * Desarrollo
    * Diseño
    * Revisar
    * Prueba
    * Desplegar

Para ver esto en un tablero, simplemente cree una etiqueta para cada etapa. Crea un tablero nuevo y agrega una lista para cada etapa. Puede arrastrar y soltar listas para ponerlas en el orden deseado

<img src="board1.png" width="500" >

# Commit

[Volver a Inicio](#menu)

* Desde nuestro repositorio agregamos un archivo nuevo 

<img src="commit1.jpg" width="500" >

* Agregamos titulo y comentario y hacemos commit al dar click en **Cometer cambios**

<img src="commit2.jpg" width="500" >

<img src="commit3.jpg" width="500" >

# Ramas
[Volver a Inicio](#menu)

* Desde nuestro repositorio agregamos una rama nueva

<img src="rama1.jpg" width="500" >

* Agregamos titulo al dar click en **Crear Rama**

<img src="rama2.jpg" width="500" >

* Y tendremos nuestra rama creada 

<img src="rama3.jpg" width="500" >
