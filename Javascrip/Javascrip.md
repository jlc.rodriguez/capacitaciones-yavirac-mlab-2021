- [volver a inicio](https://gitlab.com/jlc.rodriguez/capacitaciones-yavirac-mlab-2021/-/blob/master/README.md)

# Menu
- [Javascrip](#javascrip)
- [Dom](#dom)
- [ECMAScript](#eCMAScript)
- [La consola de JavaScript del navegador](#la_consola_de_JavaScript_del_navegador)
- [Manejo de archivos](#manejo_de_archivos)
- [Variables](#Variables)
- [Formas de Declarar Variables](#formas_de_Declarar_Variables)
- [Tipos de variables](#tipos_de_variables)
- [Los Operadores](#los_Operadores)
- [Tipos de errores](#tipos_de_errores)
- [Funciones](#funciones)
- [String](#string)

# Javascrip
 - [Ir a inicio](#menu)

JavaScript es un lenguaje de programación o de secuencias de comandos que te permite implementar funciones complejas en páginas web, cada vez que una página web hace algo más que sentarse allí y mostrar información estática para que la veas, muestra oportunas actualizaciones de contenido, mapas interactivos, animación de Gráficos 2D/3D, desplazamiento de máquinas reproductoras de vídeo, etc., puedes apostar que probablemente JavaScript está involucrado. Es la tercera capa del pastel de las tecnologías web estándar, dos de las cuales (HTML y CSS) hemos cubierto con mucho más detalle en otras partes del Área de aprendizaje.

* **HTML** es el lenguaje de marcado que usamos para estructurar y dar significado a nuestro contenido web, por ejemplo, definiendo párrafos, encabezados y tablas de datos, o insertando imágenes y videos en la página.
* **CSS** es un lenguaje de reglas de estilo que usamos para aplicar estilo a nuestro contenido HTML, por ejemplo, establecer colores de fondo y tipos de letra, y distribuir nuestro contenido en múltiples columnas.
* **JavaScript** es un lenguaje de secuencias de comandos que te permite crear contenido de actualización dinámica, controlar multimedia, animar imágenes y prácticamente todo lo demás. (Está bien, no todo, pero es sorprendente lo que puedes lograr con unas pocas líneas de código JavaScript).

# DOM
 - [Ir a inicio](#menu)

Las siglas DOM significan Document Object Model, o lo que es lo mismo, la estructura del documento HTML. Una página HTML está formada por múltiples etiquetas HTML, anidadas una dentro de otra, formando un árbol de etiquetas relacionadas entre sí, que se denomina árbol DOM (o simplemente DOM).

En Javascript, cuando nos referimos al DOM nos referimos a esta estructura, que podemos modificar de forma dinámica desde Javascript, añadiendo nuevas etiquetas, modificando o eliminando otras, cambiando sus atributos HTML, añadiendo clases, cambiando el contenido de texto

En Javascript, la forma de acceder al DOM es a través de un objeto llamado document, que representa el árbol DOM de la página o pestaña del navegador donde nos encontramos. En su interior pueden existir varios tipos de elementos, pero principalmente serán **Element** o **Node** 

<img src="mod.1.jpg" width="500" >


# ECMAScript

 - [Ir a inicio](#menu)

ECMAScript específicamente es el estándar que a partir del año 2015 a la actualidad se encarga de regir como debe ser interpretado y funcionar el lenguaje JavaScript, siendo este (JS – JavaScript) interpretado y procesado por multitud de plataformas, entre las que se encuentran los navegadores web, NodeJS u otros ambientes como el desarrollo de aplicaciones para los distintos sistemas operativos que actualmente existen en el mercado. Los responsables de dichos navegadores y JavaScript deben encargarse de interpretar el lenguaje tal como lo fija ECMAScript.

<img src="ecmas1.jpg" width="500" >

# La consola de JavaScript del navegador

 - [Ir a inicio](#menu)

Todos los navegadores web modernos incluyen una consola, una poderosa herramienta creada para desarrolladores, usuarios avanzados o cualquier persona.
Muestra mensajes de información, error o alerta que se reciben al hacer las peticiones para cargar desde la red los elementos incluidos en las páginas.
Además incluye inspectores o verdaderos depuradores de código.
También permite interactuar con la página, ejecutando expresiones o comandos de JavaScript.
El propósito es probar el funcionamiento de las páginas o aplicaciones y descubrir posibles errores en el código.
Esta consola la usamos los desarrolladores, pero cualquier aficionado puede probarla y así estar al tanto del contenido de las páginas de cualquier sitio web.

* **Iniciar y usar la consola del navegador en Firefox**

    En el navegador Firefox podemos usar las teclas Control + Mayus + J para abrir el panel de la consola.

    También podemos emplear el menú Herramientas y en Desarrollador web escoger: "Consola del navegador".

<img src="firefox1.jpg" width="500" >


* **Iniciar y usar la consola del navegador en Google Chrome**

En el navegador Google Chrome de forma similar a Firefox podemos usar las teclas Control + Mayus + J y a continuación Escape.
También podemos dar un clic en el icono del menú en la esquina superior derecha y en Herramientas escoger "Consola de JavaScript".

<img src="chrome1.jpeg" width="500" >

# Manejo de archivos

 - [Ir a inicio](#menu)

Un sitio web consta de muchos archivos: texto del contenido, código, hojas de estilo, contenido multimedia, etc. Cuando estás creando un sitio web, necesitas ensamblar estos archivos en una estructura sensible en tu computadora local, asegurarte de que puedan comunicarse entre sí y hacer que todo su contenido se vea bien antes de que eventualmente los cargues en un servidor. 

El manejo de archivos analiza algunos problemas que debes tener en cuenta, para que puedas configurar una estructura de archivos adecuada para tu sitio web.

A continuación, veamos qué estructura debería tener tu sitio de prueba. Las cosas más comunes que tendrás en cualquier proyecto de sitio web que crees son un archivo de índice HTML y directorios para contener imágenes, archivos de estilo y archivos de script

<img src="manejo_archivo1.jpg" width="500" >

<img src="manejo_archivo2.jpg" width="500" >

<img src="manejo_archivo3.jpg" width="500" >

# Variables

 - [Ir a inicio](#menu)

Una variable es un espacio en memoria donde se almacena un dato, un espacio donde podemos guardar cualquier tipo de información que necesitemos para realizar las acciones de nuestros programas. Podemos pensar en ella como una caja, donde almacenamos un dato. Esa caja tiene un nombre, para que más adelante podamos referirnos a la variable, recuperar el dato así como asignar un valor a la variable siempre que deseemos.

# Formas de Declarar Variables

 - [Ir a inicio](#menu)

* VAR: Es una variable que SI puede cambiar su valor y su scope es local.

* CONST: Es una constante la cual NO cambiara su valor en ningún momento en el futuro.

* LET: Es una variable que también podra cambiar su valor, pero solo vivirá(Funcionara) en el bloque donde fue declarada.

# Tipos de variables

 - [Ir a inicio](#menu)

* Variables tipo texto 
<img src="variabletexto1.jpg" width="500" >

* Variables numéricas
    <img src="variablesnumericas1.jpg" width="500" >

* Booleanos
    <img src="boleanos1.jpg" width="500" >

 * Listas (array, vector, arreglo)
        <img src="listas1.jpg" width="500" >

* Objetos (tuplas)
        <img src="objetos1.jpg" width="500" >

* Funciones
        <img src="funcion1.jpg" width="500" >

# Los Operadores

 - [Ir a inicio](#menu)

Un operador es un elemento de programa que se aplica a uno o varios operandos en una expresión o instrucción.

La función de cada operador depende del tipo que se esté utilizando.

 **Tipos de operadores**

* **Operadores de asignación**

Como su nombre lo dice su función es asignar un valor especifico a una variable mediante una asignación simple (=) o por dos operadores, llamados operadores compuestos.

Todos son binarios.

Es recomendable no dejar espacios entre los operadores.

<img src="asignacion1.jpg" width="500" >

* **Operadores Aritméticos**

Son aquello símbolos que nos permiten hacer operaciones o cálculos simples. Los operadores de decremento e incremento, suman o restan por defecto un 1 al valor de la variable. Suelen ser usados como mayor frecuencia en ciclos repetitivos.

Según la posición de estos operadores puede cambiar el valor:

- Si el operador esta ante de la variable se conoce como prefijo, por ende, se realiza primero el incremento o decremento y después el utilizar el valor de la variable.
- Si esta después se conoce como posfijo, primero se usa el valor de la variable y luego se incrementa o decremento.

<img src="aritmeticos1.jpg" width="500" >

* **Operadores Lógicos**

Producen un resultado booleano, les corresponden lógicos los cuales están relacionados con cierto o falso, podría decirse que ayudan a “unir” propiedades. Función de la siguiente manera.

Se tienen dos variables cualesquiera:

* Si dos ambas son verdaderas se obtiene un resultado verdadero.

* Si alguna es verdadera, es verdadero.

<img src="logicos1.jpg" width="500" >

* **Operadores de dirección**

Existen  de referencia y de in dirección,  de selección y de selección de objetos.

Los primeros se componen de alguno y de una expresión o variable unitaria.

La funcionalidad de cada uno de ellos:

* ( * ) considera a la variable u operando cono una dirección devolviendo así contenido o valor.
* (&) devuelve la dirección que ocupa en la memoria el operador.
* ( .  ) permite acceder a objetos dentro de la estructura de código.
* ( ->) permite acceder a campos para estructuras relacionadas con punteros.

<img src="direccion1.jpg" width="500" >

* **Operadores de manejo de Bits**

Son operadores que permiten hacer movimiento a nivel de bits, es decir manejar flags. Que son variables de tipo entero que puede tomas dos valores.

Estos operadores utilizan  la numeración hexadecimal que tiene  una relación directa a 4 dígitos binarios, , la cual puedes encontrar en una tabla ASCII o bien buscando una tabla especifica para esta numeración.

<img src="bits1.jpg" width="500" >

* **Operador condicional o ternario**

Existe un último operador, este a diferencia del resto es ternario, es decir utiliza tres variables u operandos es (? ) y sirve para escribir expresiones condicionales. Su formato es el siguiente

<img src="condicional1.jpg" width="500" >

expresion1 es evaluada primero, si es diferente de cero (verdadero) entonces se evalua expresion2 devolviéndose como resultado de la expresión condicional. Si expresion1 es igual a cero (falso) se evalua expresion3 y se devuelve como resultado de la expresión condicional.




# Tipos de errores

 - [Ir a inicio](#menu)

En general, cuando haces algo mal en el código, hay dos tipos principales de errores con los que te encontrarás:

* Errores de sintaxis: estos son errores de ortografía en tu código que provocan que tu programa no se ejecute en absoluto, o que deje de funcionar a mitad del camino — por lo general, también te proporcionarán algunos mensajes de error. Normalmente no es tan difícil corregirlos, ¡siempre y cuando estés familiarizado con las herramientas adecuadas y sepas qué significan los mensajes de error!

* Errores lógicos: Estos son errores en los que la sintaxis realmente es correcta pero el código no hace lo que pretendías, lo cual significa que el programa se ejecuta pero da resultados incorrectos. A menudo, estos son más difíciles de arreglar que los errores sintácticos, ya que generalmente no hay un mensaje de error que te pueda orientar hacia la fuente del error.

# Funciones 

 - [Ir a inicio](#menu)

Una función es una porción de código reusable que puede ser llamada en cualquier momento desde nuestro programa. De esta manera se evita la necesidad de estar escribiendo el mismo código una y otra vez. Esto nos ayudará  a escribir código modular en JavaScript.

JavaScript soporta todas las características necesarias para escribir código modular usando funciones. Por ejemplo, en JavaScript se tiene las siguientes funciones: alert(), write(), definidas en su núcleo una sola vez pero que son llamadas a cada momento y en cualquier parte de nuestro programa

<img src="funcion_1.jpg" width="500" >

* **Declaración de Función**

Se le asigna un nombre  a la función. Dicha función se debe cargar en el ámbito de la aplicación antes de la ejecución del código

<img src="funcion_2.jpg" width="500" >

* **Expresión de Función**

Aquí se crea una función anónima y se le asigna a una variable. Esta función no se encuentra lista hasta que esa línea en particular sea evaluada durante la ejecución del código

<img src="funcion_3.jpg" width="500" >

* **Llamado de funciones**

Para invocar una función desde cualquier parte de nuestro programa, solo es necesario escribir el nombre de la función creada seguido de paréntesis( en caso de tener parámetros indicarlos dentro, separados por comas).

<img src="funcion_4.jpg" width="500" >

* **Funciones con parámetros**

Hasta ahora sólo hemos creado funciones sin parámetros. A continuación los parámetros pueden ser capturados dentro de la función y hacer cualquier tipo de manipulación sobre estas.

Una función puede tener muchos parámetros separados por comas.

<img src="funcion_5.jpg" width="500" >

* **Funciones con la Instrucción return**

Una función JavaScript puede tener la instrucción return. Esta es requerida si queremos que la función dada nos retorne un valor. La instrucción return debe ser la última línea en una función.

<img src="funcion_6.jpg" width="500" >

# String

 - [Ir a inicio](#menu)

En javascript las variables de tipo texto son objetos de la clase String. Esto quiere decir que cada una de las variables que creamos de tipo texto tienen una serie de propiedades y métodos. Recordamos que las propiedades son las características, como por ejemplo longitud en caracteres del string y los métodos son funcionalidades, como pueden ser extraer un substring o poner el texto en mayúsculas.

Para crear un objeto de la clase String lo único que hay que hacer es asignar un texto a una variable. El texto va entre comillas, como ya hemos visto en los capítulos de sintaxis. También se puede crear un objeto string con el operador new, que veremos más adelante. La única diferencia es que en versiones de Javascript 1.0 no funcionará new para crear los Strings.

* **Propiedades de String**

Length
La clase String sólo tiene una propiedad: length, que guarda el número de caracteres del String.

* **Métodos de String**

Los objetos de la clase String tienen una buena cantidad de métodos para realizar muchas cosas interesantes. Primero vamos a ver una lista de los métodos más interesantes y luego vamos a ver otra lista de métodos menos útiles.

* **charAt(indice)**

Devuelve el carácter que hay en la posición indicada como índice. Las posiciones de un string empiezan en 0.

* **indexOf(carácter,desde)**

Devuelve la posición de la primera vez que aparece el carácter indicado por parámetro en un string. Si no encuentra el carácter en el string devuelve -1. El segundo parámetro es opcional y sirve para indicar a partir de que posición se desea que empiece la búsqueda.

* **lastIndexOf(carácter,desde)**

Busca la posición de un carácter exáctamente igual a como lo hace la función indexOf pero desde el final en lugar del principio. El segundo parámetro indica el número de caracteres desde donde se busca, igual que en indexOf.

* **replace(substring_a_buscar,nuevoStr)**

Implementado en Javascript 1.2, sirve para reemplazar porciones del texto de un string por otro texto, por ejemplo, podríamos uilizarlo para reemplazar todas las apariciones del substring "xxx" por "yyy". El método no reemplaza en el string, sino que devuelve un resultante de hacer ese reemplazo. Acepta expresiones regulares como substring a buscar.

* **split(separador)**

Este método sólo es compatible con javascript 1.1 en adelante. Sirve para crear un vector a partir de un String en el que cada elemento es la parte del String que está separada por el separador indicado por parámetro.

* **substring(inicio,fin)**

Devuelve el substring que empieza en el carácter de inicio y termina en el carácter de fin. Si intercambiamos los parámetros de inicio y fin también funciona. Simplemente nos da el substring que hay entre el carácter menor y el mayor.

* **toLowerCase()**

Pone todas los caracteres de un string en minúsculas.

* **toUpperCase()**

Pone todas los caracteres de un string en mayúsculas.

* **toString()**

Este método lo tienen todos los objetos y se usa para convertirlos en cadenas.

