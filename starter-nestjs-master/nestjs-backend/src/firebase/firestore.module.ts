import {Module} from '@nestjs/common';
import {FirebaseService} from './firebase.service';
import {FirestoreAuthService} from './auth/firestore-auth.service';
import {FirebaseUtilService} from './util/firebase-util.service';
import {UsuarioFirebaseService} from '../usuario-firebase/usuario-firebase.service';

@Module({
    providers: [
        FirebaseService,
        FirestoreAuthService,
        FirebaseUtilService,
        UsuarioFirebaseService,
    ],
    exports: [
        FirebaseService,
        FirestoreAuthService,
        FirebaseUtilService,
        UsuarioFirebaseService,
    ],
})
export class FirestoreModule {

}
