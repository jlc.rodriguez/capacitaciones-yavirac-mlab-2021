import {EntrenadorEntity} from '../../entrenador/entrenador.entity';
import {PokemonEntity} from '../../pokemon/pokemon.entity';

export const ENTITIES_BASE_RELACIONAL = [
    EntrenadorEntity,
    PokemonEntity,
];
