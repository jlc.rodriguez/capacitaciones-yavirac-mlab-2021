import {EntrenadorEntity} from '../entrenador/entrenador.entity';
import {PokemonEntity} from '../pokemon/pokemon.entity';

export interface DatosPrueba {
    entrenador?: EntrenadorEntity[];
    pokemon?: PokemonEntity[];
}
