import { Module } from '@nestjs/common';
import { PokemonController } from './pokemon.controller';
import { POKEMON_IMPORTS } from './constantes/pokemon.imports';
import { POKEMON_PROVIDERS } from './constantes/pokemon.providers';

@Module({
  imports: [...POKEMON_IMPORTS],
  providers: [...POKEMON_PROVIDERS],
  exports: [...POKEMON_PROVIDERS],
  controllers: [PokemonController],
})
export class PokemonModule {}
