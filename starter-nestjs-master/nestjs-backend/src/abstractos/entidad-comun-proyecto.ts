import {EntidadComun} from '@manticore-labs/nest-2021';
import {ActivoInactivo} from '../enums/activo-inactivo';
import {Column} from 'typeorm';

export abstract class EntidadComunProyecto extends EntidadComun {
    @Column(
        {
            name: 'SIS_HABILITADO',
            type: 'char',
            length: '1',
            comment: 'A = Activo, I = Inactivo',
            nullable: false,
            default: 'A'
        }
    )
    sisHabilitado?: ActivoInactivo;
}
