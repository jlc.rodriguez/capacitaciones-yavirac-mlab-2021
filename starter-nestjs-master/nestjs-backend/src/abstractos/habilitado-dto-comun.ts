import {EntidadComun} from '@manticore-labs/nest-2021';
import {ActivoInactivo} from '../enums/activo-inactivo';
import {IsIn, IsNotEmpty} from 'class-validator';
import {Expose} from 'class-transformer';

export abstract class HabilitadoDtoComun extends EntidadComun {
    @IsNotEmpty()
    @IsIn([
        ActivoInactivo.Inactivo,
        ActivoInactivo.Activo
    ])
    @Expose()
    sisHabilitado?: ActivoInactivo;
}
