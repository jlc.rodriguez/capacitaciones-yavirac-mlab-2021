import {Module} from '@nestjs/common';
import {CONTROLLERS} from './imports/controllers';
import {PROVIDERS} from './imports/providers';
import {IMPORTS} from './imports/imports';
import {DatosPrueba} from './interfaces/datos-prueba';
import {EntrenadorService} from './entrenador/entrenador.service';
import {CONFIG} from './environment/config';
import {ENTRENADOR_DATOS_PRUEBA} from './entrenador/constantes/entrenador.datos-prueba';
import {EntrenadorEntity} from './entrenador/entrenador.entity';
import {POKEMON_DATOS_PRUEBA} from './pokemon/constantes/pokemon.datos-prueba';
import {PokemonService} from './pokemon/pokemon.service';
import {PokemonEntity} from './pokemon/pokemon.entity';

@Module({
    imports: [
        ...IMPORTS
    ],
    controllers: [
        ...CONTROLLERS
    ],
    providers: [
        ...PROVIDERS
    ],
})
export class AppModule {
    datosPrueba: DatosPrueba = {};

    constructor(
        private readonly _entrenadorService: EntrenadorService,
        private readonly _pokemonService: PokemonService,
    ) {
        if (CONFIG.datosPrueba.crear) {
            this.crearDatosPrueba()
                .then(() => {
                    console.info({
                        mensaje: 'Termino de crear datos',
                    });
                    console.info({
                        mensaje:
                            'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
                    });
                })
                .catch((error) => {
                    console.info({
                        mensaje:
                            'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
                    });
                    console.error({
                        mensaje: 'Hubo errores.',
                        error,
                    });
                });
        } else {
            console.info({
                mensaje:
                    'Este sistema hace uso de la librería de Nestjs con propiedad intelectual de Manticore Labs con Licencia para este aplicativo.',
            });
        }
    }

    async crearDatosPrueba() {
        if (CONFIG.datosPrueba.entrenador) {
            await this.crearEntrenador11();
        }
    }

    async crearEntrenador11() {
        this.datosPrueba.entrenador = [];
        for (const datoPrueba of ENTRENADOR_DATOS_PRUEBA) {
            const respuesta = ((await this._entrenadorService.crear(
                datoPrueba(),
            )) as any) as EntrenadorEntity;
            this.datosPrueba.entrenador.push(respuesta);
        }
        if (CONFIG.datosPrueba.pokemon) {
            await this.crearPokemon21();
        }
    }

    async crearPokemon21() {
        this.datosPrueba.pokemon = [];
        for (const entrenador of this.datosPrueba.entrenador) {
            for (const datoPrueba of POKEMON_DATOS_PRUEBA) {
                const respuesta = ((await this._pokemonService.crear(
                    datoPrueba(entrenador.id),
                )) as any) as PokemonEntity;
                this.datosPrueba.pokemon.push(respuesta);
            }
        }

    }
}
