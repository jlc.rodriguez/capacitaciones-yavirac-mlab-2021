import {IoAdapter} from '@nestjs/platform-socket.io';
import * as redisIoAdapter from 'socket.io-redis';
import {CONFIG} from '../environment/config';

const redisAdapter: any = redisIoAdapter;

const adapter = redisAdapter({
    host: CONFIG.redis.REDIS_HOST,
    port: CONFIG.redis.REDIS_PORT,
    password: CONFIG.redis.REDIS_PASSWORD,
});

export class RedisIoAdapter extends IoAdapter {
    createIOServer(port: number, options?: any): any {
        const server = super.createIOServer(port, options);
        const redisAdapter = adapter({host: 'localhost', port: 6379});
        server.adapter(redisAdapter);
        return server;
    }
}
