import {Injectable} from '@nestjs/common';
import {ParametrosRequest, ServicioSeguridadComun} from '@manticore-labs/nest-2021';

@Injectable()
export class SeguridadService implements ServicioSeguridadComun {
    [key: string]: any;

    validarPermisos(request: any, parametros: ParametrosRequest, metodo: string | 'crear' | 'modificarHabilitado' | 'actualizar', id: string | undefined): Promise<boolean> {
        return Promise.resolve(true);
    }

    validarPermisosGuard(request: any): Promise<boolean> {
        return Promise.resolve(true);
    }

}
