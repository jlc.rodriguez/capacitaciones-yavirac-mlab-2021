export const CONFIG = {
    PORT: $PORT$,
    datosPrueba: {
        crear: $DP_CREAR$,
        servicioLogistico: $DP_SER_LOG$,
        conjunto: $DP_CONJ$,
        subconjuntos: $DP_SUBCONJ$,
        categoria: $CATEG$,
        tipoAtributo: $TIP_ATRI$,
        claseBien: $CLAS_BIEN$,
        catalogoAtributo: $CAT_ATRIB$,
        atributo: $ATRIB$,
        clase: $CLAS$,
        tipoMedida: $TIP_MED$,
        medida: $MED$,
    },
    oracle_relacional: {
        DB_SID: '$DB_SID$',
        DB_HOST: '$DB_HOST$',
        DB_PORT: $DB_PORT$,
        DB_TYPE: '$DB_TYPE$',
        DB_SCHEMA: '$DB_SCHEMA$',
        DB_USERNAME: '$DB_USERNAME$',
        DB_PASSWORD: '$DB_PASSWORD$',
        DB_SYNCRHONIZE: $DB_SYNCRHONIZE$,
        DB_DROPSCHEMA: $DB_DROPSCHEMA$,
        DB_CACHE: $DB_CACHE$,
        DB_DEBUG: $DB_DEBUG$,
        DB_MAX_QET: $DB_MAX_QET$,
        DB_BUFFER_ENTRIES: $DB_BUFFER_ENTRIES$,
    },
    session:{
        secreto: '$SES_SECRET$',
    },
    redis: {
        port: $RED_PORT$,
        host: '$RED_HOST$'
    },
    seguridad: {
        activar: $SEG_ACTIVAR$,
        DB_SCHEMA: '$SEG_DB_SCHEMA$',
    },
    auditoria: {
        DB_SCHEMA: '$AUD_DB_SCHEMA$',
    }
}
